
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "hin.h"
#include "http/http.h"
#include "http/cache.h"

HIN_EXPORT int httpd_parse_cache_str (const char * str, size_t len, uint32_t * flags_out, time_t * max_age) {
  string_t source, opt, param1;
  source.ptr = (char *)str;
  source.len = len;
  if (len == 0) source.len = strlen (str);
  uint32_t flags = 0;
  while (match_string (&source, "%s*([%w%-=]+)", &opt) > 0) {
    if (match_string (&opt, "max-age=%s*(%d+)", &param1) > 0) {
      if (max_age) *max_age = atoi (param1.ptr);
      flags |= HIN_CACHE_MAX_AGE;
    } else if (match_string (&opt, "public") > 0) {
      flags |= HIN_CACHE_PUBLIC;
    } else if (match_string (&opt, "private") > 0) {
      flags |= HIN_CACHE_PRIVATE;
    } else if (match_string (&opt, "immutable") > 0) {
      flags |= HIN_CACHE_IMMUTABLE;
    } else if (match_string (&opt, "no-cache") > 0) {
      flags |= HIN_CACHE_NO_CACHE;
    } else if (match_string (&opt, "no-store") > 0) {
      flags |= HIN_CACHE_NO_STORE;
    } else if (match_string (&opt, "no-transform") > 0) {
      flags |= HIN_CACHE_NO_TRANSFORM;
    } else if (match_string (&opt, "must-revalidate") > 0) {
      flags |= HIN_CACHE_MUST_REVALIDATE;
    } else if (match_string (&opt, "proxy-revalidate") > 0) {
      flags |= HIN_CACHE_PROXY_REVALIDATE;
    } else if (match_string (&opt, "no-store") > 0) {
      flags |= HIN_CACHE_NO_STORE;
    }
    if (match_string (&source, "%s*,%s*") <= 0) break;
  }

  if (flags_out) *flags_out = flags;
  return 0;
}

int header_cache_control (hin_buffer_t * buf, uint32_t flags, time_t max_age) {
  int num = 0;
  num += hin_header (buf, "Cache-Control: ");
  if (flags & HIN_CACHE_PRIVATE) num += hin_header (buf, "private, ");
  else if (flags & HIN_CACHE_PUBLIC) num += hin_header (buf, "public, ");
  if (flags & HIN_CACHE_MAX_AGE) num += hin_header (buf, "max-age=%ld, ", max_age);
  if (flags & HIN_CACHE_IMMUTABLE) num += hin_header (buf, "immutable, ");
  else if (flags & HIN_CACHE_NO_CACHE) num += hin_header (buf, "no-cache, ");
  if (flags & HIN_CACHE_NO_STORE) num += hin_header (buf, "no-store, ");
  if (flags & HIN_CACHE_NO_TRANSFORM) num += hin_header (buf, "no-transform, ");
  if (flags & HIN_CACHE_MUST_REVALIDATE) num += hin_header (buf, "must-revalidate, ");
  if (flags & HIN_CACHE_PROXY_REVALIDATE) num += hin_header (buf, "proxy-revalidate, ");

  basic_dlist_t * elem = &buf->list;
  while (elem->next) elem = elem->next;
  hin_buffer_t * last = basic_dlist_ptr (elem, offsetof (hin_buffer_t, list));
  last->count -= 2;
  hin_header (buf, "\r\n");

  return num;
}

HIN_EXPORT void hin_cache_set_number (httpd_client_t * http, time_t num) {
  http->cache = num;
  if (num > 0) {
    http->cache_flags = HIN_CACHE_PUBLIC | HIN_CACHE_IMMUTABLE | HIN_CACHE_MAX_AGE;
  } else if (num < 0) {
    http->cache_flags = HIN_CACHE_NO_STORE | HIN_CACHE_NO_CACHE;
  } else {
    http->cache_flags = 0;
  }
}


