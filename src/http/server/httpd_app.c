
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <basic_vfs.h>

#include "hin.h"
#include "http/http.h"
#include "http/cache.h"
#include "http/file.h"
#include "http/vhost.h"
#include "conf.h"

#include "hin_app.h"

void httpd_connection_close_idle () {
  basic_dlist_t * elem = hin_g.server_list.next;
  while (elem) {
    httpd_server_t * server = basic_dlist_ptr (elem, offsetof (hin_client_t, list));
    elem = elem->next;

    if ((server->s.flags & HIN_SERVER_HTTPD) == 0) continue;

    basic_dlist_t * elem1 = server->s.client_list.next;
    while (elem1) {
      httpd_client_t * http = basic_dlist_ptr (elem1, offsetof (hin_client_t, list));
      elem1 = elem1->next;

      httpd_client_shutdown (http);
    }
  }
}

httpd_server_t * httpd_create (const char * addr, const char * port, const char * sock_type, void * ssl_ctx) {

  httpd_server_t * server = calloc (1, sizeof *server);
  server->process_callback = hin_app_process_callback;
  server->finish_output_callback = hin_app_finish_output_callback;

  if (hin_httpd_start (server, addr, port, sock_type, ssl_ctx) == 0) {
    free (server);
    return NULL;
  }

  return server;
}


