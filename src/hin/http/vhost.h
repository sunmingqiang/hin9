
#ifndef HIN_HTTPD_VHOST_H
#define HIN_HTTPD_VHOST_H

#include "hin.h"
#include "http/http.h"
#include "listen.h"

#include <basic_lists.h>
#include <basic_hashtable.h>

enum {
HIN_HSTS_SUBDOMAINS = 0x1, HIN_HSTS_PRELOAD = 0x2,
HIN_HSTS_NO_REDIRECT = 0x4, HIN_HSTS_NO_HEADER = 0x8,
HIN_DIRECTORY_LISTING = 0x10, HIN_DIRECTORY_NO_REDIRECT = 0x20,
};

enum { HIN_VHOST_MAP_PREFILE = 0, HIN_VHOST_MAP_PROCESS, HIN_VHOST_MAP_FINISH, HIN_VHOST_MAP_LAST };

typedef struct httpd_server_struct {
  hin_server_t s;
  int (*begin_callback) (httpd_client_t * http);
  int (*process_callback) (httpd_client_t * http);
  int (*finish_callback) (httpd_client_t * http);
  int (*finish_output_callback) (httpd_client_t * http, hin_pipe_t * pipe);
  int (*error_callback) (httpd_client_t * http, int error_code, const char * msg);
  int (*read_callback) (hin_buffer_t * buffer, int received);
} httpd_server_t;

typedef struct hin_ssl_ctx_struct {
  int refcount;
  uint32_t magic;
  void * ctx;
  const char * cert;
  const char * key;
  struct hin_ssl_ctx_struct * next;
  basic_dlist_t list;
} hin_ssl_ctx_t;

typedef struct httpd_vhost_map_struct {
  int state;
  const char * pattern;
  struct httpd_vhost_struct * vhost;
  struct lua_State * lua;
  int callback;
  struct httpd_vhost_map_struct * next, * prev;
} httpd_vhost_map_t;

typedef struct httpd_vhost_struct {
  // callback
  int refcount;
  uint32_t magic;
  uint32_t disable;
  uint32_t debug;
  uint32_t vhost_flags;
  int timeout;
  char * hostname;
  int cwd_fd;
  void * cwd_dir;
  hin_ssl_ctx_t * ssl;
  void * ssl_ctx;
  httpd_server_t * bind;
  int hsts;
  basic_ht_t * names;

  struct lua_State *L;
  httpd_vhost_map_t * maps[HIN_VHOST_MAP_LAST];

  struct httpd_vhost_struct * parent;
  basic_dlist_t list;
} httpd_vhost_t;

int httpd_vhost_set_work_dir (httpd_vhost_t * vhost, const char * rel_path);

httpd_vhost_t * httpd_vhost_get (httpd_vhost_t * vhost, const char * name, int name_len);
httpd_vhost_t * httpd_vhost_create (const char * name, httpd_server_t * bind, httpd_vhost_t * parent);
int httpd_vhost_set_name (httpd_vhost_t * parent, const char * name, int name_len, httpd_vhost_t * new);
httpd_vhost_t * httpd_vhost_default ();
void httpd_vhost_set_debug (uint32_t debug);

#include "http.h"

int httpd_vhost_switch (httpd_client_t * http, httpd_vhost_t * vhost);
int httpd_vhost_request (httpd_client_t * http, const char * name, int len);




// temp
httpd_server_t * httpd_create (const char * addr, const char * port, const char * sock_type, void * ssl_ctx);
int hin_httpd_start (httpd_server_t * server, const char * addr, const char * port, const char * sock_type, void * ssl_ctx);
void hin_ssl_ctx_unref (hin_ssl_ctx_t * box);

#endif

