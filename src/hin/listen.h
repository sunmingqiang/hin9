
#ifndef HIN_LISTEN_H
#define HIN_LISTEN_H

#include "define.h"

enum {
HIN_SERVER_INIT=0x1, HIN_SERVER_RETRY=0x2, HIN_SERVER_HTTPD=0x4,
};

typedef struct hin_server_struct {
  hin_client_t c;
  int (*accept_callback) (hin_client_t * client);
  void * (*sni_callback) (hin_client_t * client, const char * name, int name_len);
  int (*close_callback) (hin_client_t * client);
  int (*error_callback) (hin_client_t * client);
  void * ssl_ctx;
  uint32_t flags;

  intptr_t ai_family, ai_protocol, ai_socktype;
  void * rp_base;

  int num_client;
  basic_dlist_t client_list;

  int accept_flags;
  int user_data_size;
  basic_dlist_t accept_list;

  uint32_t debug;
} hin_server_t;


int hin_socket_listen (const char * addr, const char * port, const char * sock_type, hin_server_t * ptr);
int hin_request_listen (hin_server_t * server, const char * addr, const char * port, const char * sock_type);

int hin_server_close (hin_server_t * server);

#endif

