
#ifndef HIN_H
#define HIN_H

#include <stdint.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>

#if 0
#include <basic_banned.h>
#endif

#include "define.h"
#include "system/master.h"

#define HIN_EXPORT __attribute__ ((visibility ("default")))

enum {
HNDBG_BASIC=0x1, HNDBG_CONFIG=0x2, HNDBG_VFS=0x4, HNDBG_SOCKET=0x8,
HNDBG_URING=0x10, HNDBG_SSL=0x20, HNDBG_SYSCALL=0x40, HNDBG_MEMORY=0x80,
HNDBG_HTTP=0x100, HNDBG_CGI=0x200, HNDBG_PROXY=0x400, HNDBG_HTTP_FILTER=0x800,
HNDBG_POST=0x1000, HNDBG_CHILD=0x2000, HNDBG_CACHE=0x4000, HNDBG_TIMEOUT=0x8000,
HNDBG_RW=0x10000, HNDBG_RW_ERROR=0x20000, HNDBG_PIPE=0x40000, HNDBG_INFO=0x80000,
HNDBG_PROGRESS=0x100000, HNDBG_ANNOY=0x200000, HNDBG_LAST = 0x400000,
HNDBG_ALL=0x7fffffff,
};

enum {
HIN_PIPE_DECODE = 0x1,		// nothing atm
HIN_PIPE_COUNT = 0x2,		// will the buffers added count for the byte count
HIN_PIPE_ALL = 0xff,		// all of the above
};

typedef struct {
  int fd;
  uint32_t flags;
  struct hin_ssl_struct * ssl;
  off_t pos, count;
} hin_pipe_dir_t;

struct hin_pipe_struct {
  hin_pipe_dir_t in, out;
  off_t sz;			// the amount expected to be read if HIN_COUNT
  void * parent, * parent1;
  basic_dlist_t write_que, writing, reading;
  int (*decode_callback) (hin_pipe_t * pipe, hin_buffer_t * buffer, int num, int flush);
  int (*read_callback) (hin_pipe_t * pipe, hin_buffer_t * buffer, int num, int flush);
  int (*finish_callback) (hin_pipe_t * pipe);
  int (*out_error_callback) (hin_pipe_t * pipe, int err);
  int (*in_error_callback) (hin_pipe_t * pipe, int err);
  hin_buffer_t * (*buffer_callback) (hin_pipe_t * pipe, int sz);

  int num_que;
  uint32_t flags;
  uint32_t debug;
  uint64_t hash;
  void * extra;
};

int hin_connect (const char * host, const char * port, hin_callback_t callback, void * parent, struct sockaddr * ai_addr, socklen_t * ai_addrlen);
int hin_unix_sock (const char * path, hin_callback_t callback, void * parent);

// ssl
void * hin_ssl_create_cert (const char * cert, const char * key);
void hin_ssl_free_cert (void * ctx);
int hin_ssl_connect_init (hin_client_t * client);
int hin_ssl_accept_init (hin_client_t * client);
void hin_client_ssl_cleanup (hin_client_t * client);

void hin_client_close (hin_client_t * client);

#include "liburing/compat.h"

// uring
int hin_request_write (hin_buffer_t * buffer);
int hin_request_read (hin_buffer_t * buffer);
int hin_request_accept (hin_buffer_t * buffer, int flags);
int hin_request_connect (hin_buffer_t * buffer, struct sockaddr * ai_addr, int ai_addrlen);
int hin_request_close (hin_buffer_t * buffer);
int hin_request_openat (hin_buffer_t * buffer, int dfd, const char * path, int flags, int mode);
int hin_request_statx (hin_buffer_t * buffer, int dfd, const char * path, int flags, int mask);
int hin_request_timeout (hin_buffer_t * buffer, struct __kernel_timespec * ts, int count, int flags);
int hin_request_is_overloaded ();

// pipe
hin_buffer_t * hin_pipe_get_buffer (hin_pipe_t * pipe, int sz);
int hin_pipe_init (hin_pipe_t * pipe);
int hin_pipe_start (hin_pipe_t * pipe);
int hin_pipe_advance (hin_pipe_t * pipe);
int hin_pipe_finish (hin_pipe_t * pipe);

int hin_pipe_append_raw (hin_pipe_t * pipe, hin_buffer_t * buffer);
int hin_pipe_prepend_raw (hin_pipe_t * pipe, hin_buffer_t * buf);
int hin_pipe_write_process (hin_pipe_t * pipe, hin_buffer_t * buffer, uint32_t flags);

// buffer
hin_buffer_t * hin_buffer_create_from_data (void * parent, const char * ptr, int sz);
void hin_buffer_clean (hin_buffer_t * buffer);
void hin_buffer_stop_clean (hin_buffer_t * buffer);
int hin_buffer_continue_write (hin_buffer_t * buf, int ret);

hin_buffer_t * hin_lines_create_raw (int sz);
int hin_lines_request (hin_buffer_t * buffer, int min);
int hin_lines_reread (hin_buffer_t * buf);
int hin_lines_write (hin_buffer_t * buf, char * data, int len);
int hin_lines_prepare (hin_buffer_t * buffer, int num);
int hin_lines_eat (hin_buffer_t * buffer, int num);

// header
int hin_vheader (hin_buffer_t * buffer, const char * fmt, va_list ap);
int hin_header (hin_buffer_t * buffer, const char * fmt, ...);
int hin_header_raw (hin_buffer_t * buffer, const char * data, int len);
void * hin_header_ptr (hin_buffer_t * buffer, int len);
int hin_header_date (hin_buffer_t * buffer, const char * name, time_t time);

#include <stddef.h>
#define hin_buffer_list_ptr(elem) (basic_dlist_ptr (elem, offsetof (hin_buffer_t, list)))

// timing
#include <time.h>

struct hin_timer_struct;
typedef int (*hin_timer_callback_t) (struct hin_timer_struct * timer, time_t tm);

typedef struct hin_timer_struct {
  void * ptr;
  hin_timer_callback_t callback;
  time_t time;
  basic_dlist_t list;
} hin_timer_t;

int hin_timer_update (hin_timer_t * timer, time_t new);
int hin_timer_remove (hin_timer_t * timer);

/*
#include <errno.h>
#include <stdio.h>
#include <string.h>

#define hin_error(fmt, ...) fprintf(stderr, "error! " fmt "\n", ##__VA_ARGS__)
#define hin_perror(fmt, ...) fprintf(stderr, "error! " fmt ": %s\n", ##__VA_ARGS__, strerror (errno))
#define hin_weird_error(errcode) fprintf(stderr, "error! %d\n", (errcode))
*/
void hin_error (const char * fmt, ...);
void hin_perror (const char * fmt, ...);
void hin_weird_error (int errcode);

#define hin_debug(fmt, ...) printf (fmt, ##__VA_ARGS__)
#define hin_hunt(fmt, ...) fprintf (stderr, "hunt! %s:%d %s " fmt "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__)

int hin_client_addr (char * str, int len, struct sockaddr * ai_addr, socklen_t ai_addrlen);

#endif
