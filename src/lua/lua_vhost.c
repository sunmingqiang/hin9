
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "hin.h"
#include "http/http.h"
#include "http/vhost.h"
#include "conf.h"

#include "hin_app.h"
#include "hin_lua.h"

#include <fcntl.h>

static const char * l_hin_get_str (lua_State *L, int tpos, const char * name) {
  const char * ret = NULL;
  lua_pushstring (L, name);
  lua_gettable (L, tpos);
  if (lua_type (L, -1) == LUA_TSTRING) {
    ret = lua_tostring (L, -1);
  }
  lua_pop (L, 1);
  return ret;
}

static httpd_server_t * l_hin_add_socket (lua_State *L, hin_ssl_ctx_t * box, int tpos) {
  if (lua_type (L, tpos) != LUA_TTABLE) {
    luaL_error (L, "socket not a table\n");
    return NULL;
  }
  int ssl = 0;
  const char * bind = l_hin_get_str (L, tpos, "bind");
  const char * port = l_hin_get_str (L, tpos, "port");
  const char * sock_type = l_hin_get_str (L, tpos, "sock_type");

  lua_pushstring (L, "ssl");
  lua_gettable (L, tpos);
  if (lua_type (L, -1) == LUA_TBOOLEAN) {
    ssl = lua_toboolean (L, -1);
  }
  lua_pop (L, 1);

  void * ctx = NULL;
  if (ssl) {
    if (box == NULL || box->ctx == NULL) {
      #if HIN_HTTPD_ERROR_ON_MISSING_CERT
      return luaL_error (L, "%s:%s lacks cert\n", bind, port);
      #else
      hin_error ("vhost %s:%s lacks cert", bind, port);
      #endif
      return NULL;
    }
    ctx = box->ctx;
  }

  httpd_server_t * sock = httpd_create (bind, port, sock_type, ctx);
  //sock->s.c.parent = vhost; // done at the end of the vhost creation

  return sock;
}

int l_hin_add_vhost (lua_State *L) {
  if (lua_type (L, 1) != LUA_TTABLE) {
    return luaL_error (L, "requires a table");
  }

  // TODO check if this is an actual ssl context
  httpd_vhost_t * parent = NULL;
  size_t len = 0;
  int nsocket = 0;
  const char * htdocs = NULL;
  hin_ssl_ctx_t * box = NULL;
  httpd_server_t * bind = NULL;
  httpd_vhost_t * vhost = NULL;

  lua_pushstring (L, "parent");
  lua_gettable (L, 1);
  if (lua_type (L, -1) == LUA_TLIGHTUSERDATA) {
    parent = (void*)lua_topointer (L, -1);
    if (parent->magic != HIN_VHOST_MAGIC) {
      return luaL_error (L, "parent is invalid");
    }
  }
  lua_pop (L, 1);

  lua_pushstring (L, "cert");
  lua_gettable (L, 1);
  if (lua_type (L, -1) == LUA_TBOOLEAN) {
    #if HIN_HTTPD_ERROR_ON_MISSING_CERT
    return luaL_error (L, "fatal error! cert missing\n");
    #else
    hin_error ("vhost cert missing");
    #endif
  } else if (lua_type (L, -1) == LUA_TLIGHTUSERDATA) {
    box = (void*)lua_topointer (L, -1);
    if (box) {
      if (box->magic != HIN_CERT_MAGIC) {
        return luaL_error (L, "invalid cert\n");
      }
      box->refcount++;
    }
  }
  lua_pop (L, 1);

  // listen to sockets
  lua_pushstring (L, "socket");
  lua_gettable (L, 1);
  if (lua_type (L, -1) == LUA_TTABLE) {
    int sz = hin_lua_rawlen (L, -1);
    for (int i = 1; i <= sz; i++) {
      lua_rawgeti (L, -1, i);
      httpd_server_t * sock = l_hin_add_socket (L, box, lua_gettop (L));
      if (sock) {
        nsocket++;
        bind = sock;
        sock->s.c.flags |= HIN_SYNC;
      }
      lua_pop (L, 1);
    }
  }
  lua_pop (L, 1);

  lua_pushstring (L, "host");
  lua_gettable (L, 1);
  if (lua_type (L, -1) == LUA_TTABLE) {
    int sz = hin_lua_rawlen (L, -1);

    lua_rawgeti (L, -1, 1);
    const char * name = lua_tolstring (L, -1, &len);
    if (name == NULL) {
      return luaL_error (L, "vhost missing hostname");
    }
    //vhost->hostname = strdup (name); // done in the create function
    if (hin_g.debug & HNDBG_HTTP)
      hin_debug ("vhost '%s'\n", name);

    if (httpd_vhost_get (parent, name, len)) {
      return luaL_error (L, "vhost duplicate '%s'", name);
    }
    vhost = httpd_vhost_create (name, bind, parent);
    if (vhost == NULL) {
      return luaL_error (L, "vhost unable to create");
    }
    lua_pop (L, 1);

    for (int i = 2; i <= sz; i++) {
      lua_rawgeti (L, -1, i);
      const char * name = lua_tolstring (L, -1, &len);

      if (hin_g.debug & HNDBG_HTTP)
        hin_debug ("vhost '%s'\n", name);

      if (httpd_vhost_get (vhost, name, len)) {
        return luaL_error (L, "vhost duplicate '%s'", name);
      }
      int ret = httpd_vhost_set_name (parent, name, len, vhost);
      if (ret < 0) {
        // TODO cancel the whole host
        return luaL_error (L, "vhost add '%s'\n", name);
      }
      lua_pop (L, 1);
    }
  }
  lua_pop (L, 1);

  lua_pushstring (L, "htdocs");
  lua_gettable (L, 1);
  if (lua_type (L, -1) == LUA_TSTRING) {
    htdocs = lua_tostring (L, -1);
  } else {
    htdocs = hin_app.workdir_path;
  }
  lua_pop (L, 1);

  lua_pushstring (L, "hsts");
  lua_gettable (L, 1);
  if (lua_type (L, -1) == LUA_TNUMBER) {
    vhost->hsts = lua_tonumber (L, -1);
  }
  lua_pop (L, 1);

  lua_pushstring (L, "hsts_flags");
  lua_gettable (L, 1);
  if (lua_type (L, -1) == LUA_TSTRING) {
    string_t line, param;
    line.len = 0;
    line.ptr = (char*)lua_tolstring (L, -1, &line.len);
    while (match_string (&line, "([%w_]+)%s*", &param) > 0) {
      if (match_string (&param, "subdomains") > 0) {
        vhost->vhost_flags |= HIN_HSTS_SUBDOMAINS;
      } else if (match_string (&param, "preload") > 0) {
        vhost->vhost_flags |= HIN_HSTS_PRELOAD;
      } else if (match_string (&param, "no_redirect") > 0) {
        vhost->vhost_flags |= HIN_HSTS_NO_REDIRECT;
      } else if (match_string (&param, "no_header") > 0) {
        vhost->vhost_flags |= HIN_HSTS_NO_HEADER;
      }
    }
  }
  lua_pop (L, 1);

  vhost->L = L;
  vhost->magic = HIN_VHOST_MAGIC;
  vhost->timeout = HIN_HTTPD_TIMEOUT;
  if (parent) {
    vhost->debug = parent->debug;
    vhost->disable = parent->disable;
  } else {
    vhost->debug = hin_g.debug;
  }
  vhost->parent = parent;
  lua_pushlightuserdata (L, vhost);

  httpd_vhost_set_work_dir (vhost, htdocs);

  if (box) {
    vhost->ssl = box;
    vhost->ssl_ctx = box->ctx;
  }

  // add appropriate parent to sockets
  basic_dlist_t * elem = hin_g.server_list.next;
  while (elem) {
    basic_dlist_t * next = elem->next;
    hin_client_t * client = basic_dlist_ptr (elem, offsetof (hin_client_t, list));
    if (client->flags & HIN_SYNC) {
      client->flags &= ~HIN_SYNC;
      client->parent = vhost;
    }
    elem = next;
  }

  return 1;
}

int hin_bind_default_vhost (httpd_server_t * sock) {
  httpd_vhost_t * vhost = httpd_vhost_default ();
  if (vhost) {
    sock->s.c.parent = vhost;
    return 0;
  }
  vhost = httpd_vhost_create ("localhost", sock, NULL);
  httpd_vhost_set_work_dir (vhost, ".");
  return 0;
}

