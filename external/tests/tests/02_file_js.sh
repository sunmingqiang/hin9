
set -e

export BENCH_CON=10
export BENCH_NUM=50

export URL_PATH=$BENCH_HTDOCS_TEST_DIR/large.txt
export LOCAL_PATH=$BENCH_HTDOCS_DIR/$BENCH_HTDOCS_TEST_DIR/large.txt
export SUBTEST="normal ssl deflate gzip range head hammer"

sh $TOOL_DIR/request.sh

