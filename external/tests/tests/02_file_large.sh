
set -e

export BENCH_CON=10
export BENCH_NUM=50

export URL_PATH=$BENCH_HTDOCS_TEST_DIR/large.bin
export LOCAL_PATH=$BENCH_HTDOCS_DIR/$BENCH_HTDOCS_TEST_DIR/large.bin
export SUBTEST="normal ssl deflate gzip range"

# head hammer # TODO temporarily disabled due to taking too long

sh $TOOL_DIR/request.sh

