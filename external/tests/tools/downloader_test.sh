
check_output () {
  echo "Start test $name $module"

  url="${HOST_URL}${file_url}"
  out_file="$BENCH_BINARY_DIR/${name}_${module}.bin"
  local_path="$BENCH_HTDOCS_DIR/$file_path"

  echo "Download $url to $out_file '${HOST_URL}:${file_url}'"

  $ROOT_DIR/build/$BENCH_BINARY_NAME -V -do $url $out_file
  RET_CODE=$?

  if [ $RET_CODE -ne 0 ]; then
    echo "command failed"
    exit 1
  fi

  md51=`md5sum $out_file | awk '{ print $1 }'`
  md52=`md5sum "$local_path" | awk '{ print $1 }'`

  printf "$out_file: $md51\n$local_path: $md52\n"

  if [ "$md51" != "$md52" ]; then
    echo "Output doesn't match!"
    exit 1
  fi

  echo "Completed test $name $module"
}

check_output
