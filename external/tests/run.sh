#!/bin/bash

# tests documentation in top root docs folder

CWD_DIR=`pwd`
export SCRIPT_DIR="$( realpath ` dirname -- "$0" ` )/"
export ROOT_DIR="$( realpath $SCRIPT_DIR/../../ )/"

export BENCH_HOST=${BENCH_HOST:-localhost}
export BENCH_PORT=${BENCH_PORT:-8080}
export BENCH_PORTS=${BENCH_PORTS:-8081}
export BENCH_REMOTE=${BENCH_REMOTE:-http://localhost:28082/}
export BENCH_CON=${BENCH_CON:-1000}
export BENCH_NUM=${BENCH_NUM:-10000}

export BENCH_ROOT_DIR=${BENCH_ROOT_DIR:-${ROOT_DIR}/build/tests/}
export TOOL_DIR=${SCRIPT_DIR}/tools/
export CONFIG_DIR=${BENCH_ROOT_DIR}config/
export BENCH_HTDOCS_DIR=${BENCH_HTDOCS_DIR:-${ROOT_DIR}/htdocs/}
export BENCH_WORK_DIR=${BENCH_WORK_DIR:-${ROOT_DIR}/workdir/}
export BENCH_RESULTS_DIR=${BENCH_RESULTS_DIR:-${BENCH_ROOT_DIR}logs/}
export BENCH_TEST_LOG_DIR=${BENCH_TEST_LOG_DIR:-${BENCH_ROOT_DIR}tests/}
export BENCH_BINARY_DIR=${BENCH_BINARY_DIR:-${BENCH_ROOT_DIR}downloads}
export BENCH_CURL_FLAGS=${BENCH_CURL_FLAGS:-"-v -k --fail"}
export BENCH_BINARY_NAME=${BENCH_BINARY_NAME:-"hinsightd"}
export BENCH_HTDOCS_TEST_DIR="bin"

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

complete=0
total=0
server_crashed=0

mkdir -p $BENCH_ROOT_DIR/{config/config,downloads,logs,tests} ${BENCH_WORK_DIR}/ssl

cp $BENCH_WORK_DIR/{main.lua,lib.lua} $CONFIG_DIR/
cp $BENCH_WORK_DIR/config/{00_defines.lua,10_localhost.lua,99_default.lua} $CONFIG_DIR/config/
printf "server_remote = \"$BENCH_REMOTE\"\n" > $CONFIG_DIR/config/20_test_temp.lua
cat $BENCH_WORK_DIR/config/_20_*.lua >> $CONFIG_DIR/config/20_test_temp.lua

if [ ! -f ${BENCH_WORK_DIR}/ssl/key.pem ]; then
  openssl req -x509 -newkey rsa:2048 -keyout ${BENCH_WORK_DIR}/ssl/key.pem -out ${BENCH_WORK_DIR}/ssl/cert.pem -sha256 -days 365 -nodes -subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=example.com"
fi

$TOOL_DIR/temp_files.sh
rm -f ${BENCH_TEST_LOG_DIR}/access.log

${ROOT_DIR}build/$BENCH_BINARY_NAME --workdir ${ROOT_DIR} --config ${CONFIG_DIR}main.lua --log ${BENCH_RESULTS_DIR}server.log --logdir ${BENCH_TEST_LOG_DIR} &
PID=$!

sleep 1

printf "\ntesting $BENCH_HOST:$BENCH_PORT REMOTE $BENCH_REMOTE with -c $BENCH_CON -n $BENCH_NUM on `date`\n" >> $BENCH_RESULTS_DIR/bench.txt

run_test () {
  export name=`basename $file`
  export name="${name%.*}"
  export test_dir=$BENCH_ROOT_DIR/
  ((total++))
  echo "Test $name started on `date`" &> ${BENCH_TEST_LOG_DIR}/$name.log
  printf "run  \t$name"
  sh $file &>> ${BENCH_TEST_LOG_DIR}$name.log
  exit_code=$?
  if ! kill -s 0 $PID &>> ${BENCH_RESULTS_DIR}/server.log; then
    exit_code=1
    echo "Server crashed" >> ${BENCH_TEST_LOG_DIR}/$name.log
    if [ $server_crashed -eq 0 ]; then
      printf "\nServer ${RED}crashed${NC} !!!\n"
      server_crashed=1
    fi
  fi
  if [ $exit_code -eq 0 ]; then
    printf "\r${GREEN}success$NC\t$name\n"
    ((complete++))
  else
    printf "\r${RED}failed$NC\t$name (more info 'tail ${BENCH_TEST_LOG_DIR}$name.log')\n"
  fi
}

if [ -n "$1" ]; then
  for fn in "$@"
  do
    file=$CWD_DIR/$fn
    run_test
  done
else
  for file in $SCRIPT_DIR/tests/*.sh; do
    run_test
  done
fi

kill -2 $PID &>> ${BENCH_RESULTS_DIR}/server.log

echo "Successfully finished $complete/$total tests"

if [ $complete != $total ]; then
  exit $(($total-$complete))
fi

wait
