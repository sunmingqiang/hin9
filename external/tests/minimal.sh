#!/bin/bash

# tests documentation in top root docs folder

export ROOT_DIR="$( realpath ` dirname -- "$0" ` )"

cd $ROOT_DIR/tests

$ROOT_DIR/run.sh 00_*.sh 02_file_*.sh 05_*.sh 06_*.sh 09_*.sh 10_hammer_fds.sh

