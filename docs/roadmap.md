
* bugs
  * only unreported ones

* cache gzipped files

* sandboxing
  * syscall filtering
  * namespaces

* one day
  * traffic shaping
  * refactor cache subsystem
  * wildcard vhosts
  * better error handling
  * cgi spec compliance
  * threads

